QT += core sql
QT -= gui

CONFIG += c++11

TARGET = sqltest
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp
