#include <QCoreApplication>
#include <QFile>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QStandardPaths>
#include <QDir>
#include <QDebug>
#include <QSqlQuery>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    QDir pathToDB(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
    if (!pathToDB.exists()) {
        pathToDB.mkpath(pathToDB.absolutePath());
        qDebug() << "directory's created";
    }
    QString absolutePathToDB = pathToDB.absoluteFilePath("test");
    //qDebug() << QStandardPaths::displayName(QStandardPaths::AppDataLocation) + QDir::separator() + QString("youAreGreatWithBOS");
    //bool DBExists = QFile::exists(absolutePathToDB);
    db.setDatabaseName(absolutePathToDB);
    //qDebug() << db.databaseName() << " is database name";
    if (!db.open()) {
        qDebug() << "Cannot open database";
        qDebug() << "Unable to establish a database connection.\n"
                    "This example needs SQLite support. Please read "
                    "the Qt SQL driver documentation for information how "
                    "to build it.\n\n"
                    "Click Cancel to exit.";
    }
    QSqlQuery query;
    bool ok = query.exec("CREATE TABLE RemoteWorkerData (varName TEXT,"
                         "value BLOB)");
    if (!ok) {
        qDebug() << "table RemoteWorkerData is not created";

    } else {
        qDebug() << "all right";
    }

    return a.exec();
}
